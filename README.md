# Lascaux

Lascaux is a web application designed and developed for [Acadiana Center for the Arts](http://acadianacenterforthearts.org).







### Goals:

- Introduce a more standardized UI/UX to current users.
- Cross-Platform Compatibility 





### Project Roadmap



#### [Node.js](http://httpwg.org/specs/rfc7540.html)

Abandonment of [PHP](http://php.net) in favor of [Node.js](https://github.com/nodejs/node).

**Overview**

- Built on Google's [Chrome V8](https://developers.google.com/v8/) Javascript engine.
- Non-render blocking
- Lightweight

**Benefits of implementation**

- *Small system footprint*. The interpreter of Node.js,  allocates significantly fewer system resources than it's PHP counterpart. 
- *Event Driven I/O*: 



#### NoSQL

Abandon traditional relational database management systems (RDBMS) such as [MariaDB](https://github.com/MariaDB/server), [PostgreSQL](https://github.com/postgres/postgres),  in favor of of a NoSQL implementation such as [MongoDB](https://github.com/mongodb/mongo), [CouchDB](http://couchdb.apache.org/), [OrientDB](https://github.com/orientechnologies/orientdb), or [Neo4J](https://neo4j.com)



**Differences**

- *Data Models*
- *Data Structures*: The models of relational databases are constructed to handle relational data. Often in these cases, the data can be so abstracted it looses meaning without the relationship definitions. By contrast, NoSQL databases are designed to handle unstructured data.
- *Scalability*: Relational database models require a singular server. 
- Open source



#### GraphQL





#### [HTTP/2](https://http2.github.io/) Protocol

HTTP/2 is the first major revision of the HTTP protocol in over 15 years. It's current state is adapted from the [SPDY](https://www.chromium.org/spdy/spdy-whitepaper) protocol, developed by Google.



- **Benefits**
  - *Multiplexing*: Allows for the parallel loading of client requested assets over a single TCP connection.
  - *Push*: Allows for speculative loading of assets prior to requested yielding a reduction in page load times by reducing the number of server requests.
  - *Stream Priority*: a client can assign a priority for a new stream. By of the assets required to load the page.
  - *Compression*: All request headers are sent in a compressed format, reducing the amount of  duplicated data transferred.
  - *Security*: Default connection security via TLS. Though not required by the protocol itself, major clients only implement the protocol using TLS

- **Resources and Further Reading**

  - [HTTP/2 Refspec: RFC7540](http://httpwg.org/specs/rfc7540.html) 
  - [Google: Introduction to HTTP/2](https://developers.google.com/web/fundamentals/performance/http2/)

  ​



