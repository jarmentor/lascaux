<?php
/**
 * Plugin Name:     Warhol
 * Plugin URI:      https://lamplight.studio/
 * Description:     Andy Warhol looks a scream
 * Author:          Jonathan Armentor
 * Author URI:      YOUR SITE HERE
 * Text Domain:     warhol
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Warhol
 */
